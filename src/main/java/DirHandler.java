import java.io.File;
import java.io.IOException;

/**
 * Created by Doomsday Device on 24.09.2016.
 */
public class DirHandler
{
    private static File directory;

    // Copy files for some conditions
    public static void copyPartsToDiff(File source_dir) throws IOException
    {
        System.out.println("Copying...");
        directory = source_dir;
        if (directory.exists()){
            checkContent(directory);
        }
        else System.out.println("Directory not found...");
    }
    //recursive procedure for finding the contents of a directory and start filter

    private static void checkContent(File directory) throws IOException
    {
        if (directory.isFile()) {
            if (!Conditions.passTheConditions(directory, InitDir.target_dir)) {
                if(InitDir.antivirus) {
                    try
                    {
                        if (!Antivirus.isItVirus(directory.getAbsoluteFile(), InitDir.byteSequence))
                        {
                            FileHandler.copyFile(directory.getAbsoluteFile(), InitDir.diff_dir);
                        }
                    } catch (SecurityException e) {
                        System.out.println("File " + directory.getAbsolutePath() + " contains virus. Copying a file is not made.");
                    }

                } else {
                    FileHandler.copyFile(directory.getAbsoluteFile(), InitDir.diff_dir);
                }
            }
        }
        else {
            File[] sub_directory = directory.listFiles();
            for (File sub_path : sub_directory)
                checkContent(sub_path);
        }
    }

}
