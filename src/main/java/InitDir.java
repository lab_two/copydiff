import java.io.File;
import java.util.ArrayList;

/**
 * Created by Doomsday Device on 24.09.2016.
 */
public class InitDir
{
    public static File source_dir;
    public static File target_dir;
    public static File diff_dir;
    public static File byteSequence;
    public static boolean antivirus = false;
    public static ArrayList<File> filePathList = new ArrayList<File>();

    public static void initDir()
    {
        System.out.println("");
        System.out.println("Write the path to the source_dir: ");
        source_dir = new File(FileHandler.readPath());
        source_dir = source_dir.getAbsoluteFile();
        //default // source_dir = new File("D:\\Java\\CopyDiff\\tmp\\source_dir");

        System.out.println("");
        System.out.println("Write the path to the target_dir: ");
        target_dir = new File(FileHandler.readPath());
        target_dir = target_dir.getAbsoluteFile();
        //default // target_dir = new File("D:\\Java\\CopyDiff\\tmp\\target_dir");

        System.out.println("");
        System.out.println("Write the path to the diff_dir: ");
        diff_dir = new File(FileHandler.readPath());
        diff_dir = diff_dir.getAbsoluteFile();
        //default // diff_dir = new File("D:\\Java\\CopyDiff\\tmp\\diff_dir\\");

        System.out.println("");
        byteSequence = new File(Antivirus.readPathVirus());
        byteSequence = byteSequence.getAbsoluteFile();
        //default // byteSequence = new File("D:\\Java\\CopyDiff\\tmp\\byteSequence.txt");

    }
}

