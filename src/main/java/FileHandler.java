import java.io.*;
import java.nio.file.Files;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

/**
 * Created by Doomsday Device on 24.09.2016.
 */
public class FileHandler
{
    private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void copyFile(File source, File dest)
    {
        String str = source.toString();
        String[] splitLine = str.split("source_dir", 2);
        String openPathSource = splitLine[0];
        String closePathSource = splitLine[1];

        StringBuilder destinationPath = new StringBuilder(dest.toString());
        destinationPath.append(closePathSource);
        File finalDest = new File(destinationPath.toString());
        String d = finalDest.getParent();
        splitLine = d.split("diff_dir", 2);
        String openPathDest = splitLine[0];
        String closePathDest = splitLine[1];
        new File(openPathDest + "diff_dir" + closePathDest).mkdirs();

        try {
            Files.copy(source.toPath(), finalDest.toPath());
        } catch (IOException e) {
            System.out.println("File found in diff_dir. Replaced : " + finalDest);
            finalDest.delete();
            try {
                Files.copy(source.toPath(), finalDest.toPath());
            } catch (IOException err) {
                err.printStackTrace();
                System.exit(1);
            }
        }
    }

    public static String readPath()
    {
        String dir = "";
        for(;;)
        {
            try
            {
                dir = br.readLine();
                if(new File(dir).isDirectory()) {
                    break;
                } else {
                    throw new IOException();
                }
            } catch (IOException e)
            {
                System.out.println("Wrong path. Please try again:");
            }
        }
        return dir;
    }



    public static String readLineFile(File source)
    {
        StringBuilder file = new StringBuilder("");
        try {
            try {
                BufferedReader fin = new BufferedReader(new FileReader(source));
                String line;
                while ((line = fin.readLine()) != null) {
                    file.append(line);
                }
            } catch (FileNotFoundException e) {
                System.err.println("File not found.");
                System.exit(1);
            }
        } catch (IOException e){
            e.printStackTrace();
            System.exit(1);
        }
        return file.toString();
    }

    public static String readBinaryFile (File source){
        StringBuilder file = new StringBuilder("");
        try {
            try {
                FileReader reader = new FileReader(source);
                int buff;
                while ((buff = reader.read()) > 0) {
                    file.append(buff);
                }
            } catch (FileNotFoundException e) {
                System.err.println("File not found.");
                System.exit(1);
            }
        } catch (IOException e){
            e.printStackTrace();
            System.exit(1);
        }
        return file.toString();
    }

    public static File makeFile(File source_file, File dist_dir, String regex) {
        String str = source_file.toString();
        String[] splitLine = str.split(regex, 2);
        String openPathSource = splitLine[0];
        String closePathSource = splitLine[1];
        StringBuilder sbDistPath = new StringBuilder(dist_dir.toString());
        sbDistPath.append(closePathSource);

        File dist_file =  new File(sbDistPath.toString());

        return dist_file;
    }

    public static long doChecksum(File fileName) {
        long checksum = 0;
        try {
            CheckedInputStream cis = null;
            long fileSize = 0;
            try {
                // Computer CRC32 checksum
                cis = new CheckedInputStream(
                        new FileInputStream(fileName), new CRC32());
                fileSize = fileName.length();
            } catch (FileNotFoundException e) {
                System.err.println("File not found.");
                System.exit(1);
            }
            byte[] buf = new byte[128];
            while(cis.read(buf) >= 0) {
            }
            checksum = cis.getChecksum().getValue();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return checksum;
    }
}
