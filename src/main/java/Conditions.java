import java.io.File;
import java.io.IOException;

/**
 * Created by Doomsday Device on 24.09.2016.
 */
public class Conditions
{
    //Проверка на одно условие
    public static boolean passTheCondition(File source_file, File target_dir)
    {
        File target_file = FileHandler.makeFile(source_file, target_dir, "source_dir");

        if (isItExists(source_file, target_file))
        {
            return false;
        } else return true;

    }
    //Проверка на множество условий
    public static boolean passTheConditions(File source_file, File target_dir) throws IOException
    {
        File target_file = FileHandler.makeFile(source_file, target_dir, "source_dir");

        if (!isItExists(source_file, target_file)){
            return false;
        }
        else if (!compareSize(source_file, target_file)){
            return false;
        }
        else if (compareData(source_file, target_file)){
            return true;
        } else if(compareCheckSum(source_file, target_file)) {
            return true;
        }
        return false;
    }

    public static boolean isItExists(File source_file, File target_file) {
        if (target_file.isFile()) {
            return true;
        } else return false;
    }

    public static boolean compareSize(File source_file, File target_file) {
        if(source_file.length() == target_file.length()) {
            return true;
        }
        else return false;
    }

    public static boolean compareData (File source_file, File target_file) {
        if(FileHandler.readLineFile(source_file).equals(FileHandler.readLineFile(target_file))) {
            return true;
        }
        return false;
    }

    public static boolean compareCheckSum(File source_file, File target_file) {
        Long source = FileHandler.doChecksum(source_file);
        Long target = FileHandler.doChecksum(target_file);
        if(source.equals(target)) {
            return true;
        }
        else return false;
    }
}
