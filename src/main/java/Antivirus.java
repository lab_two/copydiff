﻿import java.io.*;

/**
 * Created by Doomsday Device on 25.09.2016.
 */
public class Antivirus
{
    //byteSequence is taked from file
    private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static boolean isItVirus(File source_file, File byteSequence) {
        StringBuilder sbSource = new StringBuilder(FileHandler.readBinaryFile(source_file));
        StringBuilder sbVirus = new StringBuilder(FileHandler.readBinaryFile(byteSequence));

        if(sbSource.toString().contains(sbVirus.toString())) {
            throw new SecurityException();
        }
        else return false;
    }

    public static String readPathVirus() {
        String dir = "";
        System.out.println("Enable antivirus? y/n");
        for(;;)
        {
            try
            {
                dir = br.readLine();
                if(dir.equals("y")) {
                    InitDir.antivirus = true;
                    for(;;)
                    {
                        try {
                            System.out.println("Write the path to the file with sequence of bytes: ");
                            dir = br.readLine();
                            if(new File(dir).isFile()){break;}
                            else {throw new IOException();}
                        } catch (IOException e) {
                            System.out.println("Wrong file. Please try again:");
                        }
                    }
                    break;
                } else if (dir.equals("n")) {break;}
                else {throw new IOException();}
            } catch (IOException e) {
                System.out.println("Please write 'y' or 'n':");
            }
        }
        return dir;
    }

}
